import threadPoolsClasses.FixedThreadPool;
import threadPoolsClasses.ScalableThreadPool;
import threadPoolsClasses.thread.Task;

/**
 * Главный класс для запуска приложения.
 * @autor Печенин Вадим
 * @version 1.0
 */
public class Main {
    public static void main(String[] args){
        testScalableThreadPool();
    }

    public static void testFixedThreadPool(){
        FixedThreadPool pool = new FixedThreadPool(7);
        pool.start();
        for (int i = 0; i < 5; i++) {
            Task task = new Task(i);
            pool.execute(task);
        }
    }

    public static void testScalableThreadPool(){
        ScalableThreadPool pool = new ScalableThreadPool(2,8);
        pool.start();
        for (int i = 0; i < 20; i++) {
            Task task = new Task(i);
            pool.execute(task);
        }
    }

}
