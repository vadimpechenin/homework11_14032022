package threadPoolsClasses.thread;

/** Поток задания, реализующий Runnable.
 * Код взят частично из книги Н.А. Вязовика "Программирование на Java"
 * @autor Печенин Вадим
 * @version 1.0
 */
public class Task implements Runnable{
    private int num;

    public Task(int n) {
        //Номер для идентификации потока
        num = n;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void run() {
        //некоторое долгое действие, вычисление
/*        long sum = 0;
        for (int i = 0; i < 1000; i++){
            sum+=i;
        }*/
        System.out.println("Задание № " + this.getNum() + " добавлено в очередь");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
