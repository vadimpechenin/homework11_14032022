package threadPoolsClasses;

import threadPoolsClasses.baseInterface.ThreadPool;
import threadPoolsClasses.thread.Task;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/** Класс-реализация ThreadPool.
 * В конструкторе задается минимальное и максимальное(int min, int max) число
 * потоков,
 * количество запущенных потоков может быть увеличено от минимального к максимальному, если при
 * добавлении нового задания в очередь нет свободного потока для исполнения этого задания. При отсутствии
 * задания в очереди, количество потоков опять должно быть уменьшено до значения min
 * Для формирования блокирующих очередей взята стандартная реализация
 * Базовая идея взята с https://coderlessons.com/articles/java/kak-realizovat-pul-potokov-v-java
 * https://habr.com/ru/post/326146/
 * @autor Печенин Вадим
 * @version 1.0
 */
public class ScalableThreadPool implements ThreadPool{
    private final int minNumberOfThreads;
    private final int maxNumberOfThreads;
    private int countOfThreads;
    private final MyThread[] threads;
    private final LinkedBlockingQueue queue;

    public ScalableThreadPool(int minNumberOfThreads, int maxNumberOfThreads) {
        this.minNumberOfThreads = minNumberOfThreads;
        this.maxNumberOfThreads = maxNumberOfThreads;
        this.countOfThreads = minNumberOfThreads;
        this.threads = new MyThread[maxNumberOfThreads];
        this.queue = new LinkedBlockingQueue();
    }

    public void start() {
        for (int i = 0; i < minNumberOfThreads; i++) {
            // Запуск потока
            threads[i] = new MyThread(i, minNumberOfThreads);
            threads[i].start();
        }
    }

    public void execute(Runnable runnable) {
        synchronized (queue) {
            queue.add(runnable);
            //Пробуждаем один случайно выбранный поток из данного набора
            queue.notify();
            //Добавление и запуск нового потока если очередь не пуста и максимальное число потоков не достигнуто
            if ((!queue.isEmpty())&(countOfThreads<maxNumberOfThreads)){
                System.out.println("Добавлен поток № " +Integer.toString(countOfThreads));
                threads[countOfThreads] = new MyThread(countOfThreads, minNumberOfThreads);
                threads[countOfThreads].start();
                countOfThreads+=1;
            }
        }
    }

    private class MyThread extends Thread {
        private final int name;
        private final int minNumberOfThreads;
        //Прерывания для завершения потока
        private boolean isActive;

        public MyThread(int name, int minNumberOfThreads) {
            this.name = name;
            this.minNumberOfThreads = minNumberOfThreads;
            this.isActive=true;
        }
        //Метод для завершения потока
        void disable(){
            isActive=false;
        }

        public void run() {
            Runnable task;
            //Цикл выполнения задания
            loop: while (isActive) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait(9);
                        } catch (InterruptedException e) {
                            System.out.println("Произошла ошибка во время ожидания очереди: " + e.getMessage());
                        }
                        if (queue.isEmpty()&&(name>minNumberOfThreads-1)){
                            //Выход из цикла
                            System.out.println("Убран поток № " +Integer.toString(this.name));
                            this.disable();
                            break loop;
                        }
                    }
                    //Забираем задание в работу
                    task = (Runnable) queue.poll();
                }

                try {
                    //Забираем задание в работу
                    System.out.println("Задание № " + ((Task) task).getNum() + " выполняется потоком №" + Integer.toString(this.name));
                    task.run();
                } catch (RuntimeException e) {
                    //Ловим ошибку чтобы пул не потерял потоки
                    System.out.println("Пул потоков прерван из-за проблемы: " + e.getMessage());
                }
            }
        }
    }
}
