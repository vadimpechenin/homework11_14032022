package threadPoolsClasses;

import threadPoolsClasses.baseInterface.ThreadPool;
import threadPoolsClasses.thread.Task;

import java.util.concurrent.LinkedBlockingQueue;

/** Класс-реализация ThreadPool.
 * Количество потоков задается в конструкторе и не меняется.
 * Для формирования блокирующих очередей взята стандартная реализация
 * Базовая идея взята с https://coderlessons.com/articles/java/kak-realizovat-pul-potokov-v-java
 * @autor Печенин Вадим
 * @version 1.0
 */
public class FixedThreadPool implements ThreadPool {
    private final int numberOfThreads;
    private final MyThread[] threads;
    private final LinkedBlockingQueue queue;

    public FixedThreadPool(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
        this.threads = new MyThread[numberOfThreads];
        this.queue = new LinkedBlockingQueue();
    }

    public void start() {
        for (int i = 0; i < numberOfThreads; i++) {
            // Запуск потока
            threads[i] = new MyThread(i);
            threads[i].start();
        }
    }

    public void execute(Runnable runnable) {
        synchronized (queue) {
            queue.add(runnable);
            //Пробуждаем один случайно выбранный поток из данного набора
            queue.notify();
        }
    }

    private class MyThread extends Thread {
        private final String name;

        public MyThread(int name) {
            this.name = Integer.toString(name);
        }

        public void run() {
            Runnable task;
            //Цикл выполнения задания
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("Произошла ошибка во время ожидания очереди: " + e.getMessage());
                        }
                    }
                    //Забираем задание в работу
                    task = (Runnable) queue.poll();
                }

                try {

                    //Забираем задание в работу
                    System.out.println("Задание № " + ((Task)task).getNum() + " выполняется потоком №" + this.name);
                    task.run();
                } catch (RuntimeException e) {
                    //Ловим ошибку чтобы пул не потерял потоки
                    System.out.println("Пул потоков прерван из-за проблемы: " + e.getMessage());
                }
            }
        }
    }
}
