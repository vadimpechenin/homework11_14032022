import org.testng.annotations.*;

/**
 * Класс для тестирования методов класса ScalableThreadPool.
 * @autor Печенин Вадим
 * @version 1.0
 */
public class ScalableThreadTest {
    //Класс собственно для тестирования разработанного фукнционала

    @BeforeSuite
    public void create2() {
        System.out.println("****Тесты для методов класса ScalableThreadPool****");
    }

    @Test
    public void test2() throws InterruptedException {
        Main.testScalableThreadPool();
        Thread.sleep(10000);
    }

    @AfterSuite
    public void reportReady2() {
        System.out.println("****Завершены тесты для методов класса ScalableThreadPool****");
    }
}
