
import org.testng.annotations.*;


import java.util.ArrayList;
import java.util.Iterator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

/**
 * Класс для тестирования методов класса FixesThreadPool.
 * @autor Печенин Вадим
 * @version 1.0
 */
public class FixedThreadTest {
    //Класс собственно для тестирования разработанного фукнционала

    @BeforeSuite
    public void create1() {
        System.out.println("****Тесты для методов класса FixesThreadPool****");

    }

    @Test
    public void test1() throws InterruptedException {
        Main.testFixedThreadPool();
        Thread.sleep(10000);
    }

    @AfterSuite
    public void reportReady1() {
        System.out.println("****Завершены тесты для методов класса FixesThreadPool****");
    }
}
